import argparse

from knock.knock import send


def main():
    # Get command line options using argparse
    # Create the main parser
    parser = argparse.ArgumentParser(
        description='Send a knock to a machine running knockserver.')

    # Add parsing for the 'send' command
    parser.add_argument('host', metavar='hostname/ip',
                        help='a host that is running knockserver')
    parser.add_argument('port', metavar='port', type=int,
                        help='the port that knockserver is listening on')
    parser.add_argument('-c', '--command', metavar='command',
                        help='the command to send to knockserver', default='knock')
    parser.add_argument('-t', '--timeout', metavar='timeout',
                        help='the time to wait before retrying in ms', default=100, type=int)
    parser.add_argument('-r', '--retries', metavar='retries',
                        help='the number of retries', default=10, type=int)
    parser.add_argument('-a', '--age', metavar='age',
                        help='the maximum age of a received ack in s', default=5, type=int)
    parser.add_argument('-p', '--password', metavar='password',
                        help='password for encrypting knock')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='display verbose runtime information')
    parser.set_defaults(func=send)

    # Execute the selected command
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
