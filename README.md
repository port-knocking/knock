# Knock - Networks I Project
A python port knocking utility written by J.T. Cullen and Hetvi Mehta.
## Usage
```
$ knock -h
usage: knock [-h] [-c command] [-t timeout] [-r retries] [-a age]
             [-p password] [-v]
             hostname/ip port

Send a knock to a machine running knockserver.

positional arguments:
  hostname/ip           a host that is running knockserver
  port                  the port that knockserver is listening on

optional arguments:
  -h, --help            show this help message and exit
  -c command, --command command
                        the command to send to knockserver
  -t timeout, --timeout timeout
                        the time to wait before retrying in ms
  -r retries, --retries retries
                        the number of retries
  -a age, --age age     the maximum age of a received ack in s
  -p password, --password password
                        password for encrypting knock
  -v, --verbose         display verbose runtime information
```
```
$ knockserver -h
usage: knockserver [-h] <command> ...

Run a server to look out for and react to knocks.

positional arguments:
  <command>
    run           Run the knockserver
    add-client    Add the key for a knock client
    remove-client
                  Remove the key for a knock client
    add-command   Add a command to execute upon successful knock
    remove-command
                  Remove a configured command

optional arguments:
  -h, --help      show this help message and exit
```
## Project Set Up
Install [Python](https://www.python.org/) 3.7  
Install [Pipenv](https://github.com/pypa/pipenv) using [Homebrew](https://brew.sh/):
```
$ brew install pipenv
```
Or using [Pip](https://www.python.org/):
```
$ pip install --user pipenv
```
Clone the repository locally:
```
$ git clone https://gitlab.com/port-knocking/knock.git
```
Switch to the project directory:
```
$ cd knock
```
Set up the [Pipenv](https://github.com/pypa/pipenv) environment (This installs knock/knockserver to a virtual python environment, so they dont screw up your system install if something goes wrong):
```
$ pipenv install --dev
```
Launch the [Pipenv](https://github.com/pypa/pipenv) shell (You will need to do this each time you launch a new terminal in order to access the knock/knockserver commands):
```
$ pipenv shell
```
You should now be able to run the knock and knockserver commands from the command line (Like a C program):
```
$ knock
usage: knock [-h] [-c command] [-t timeout] [-r retries] [-a age]
             [-p password] [-v]
             hostname/ip port
knock: error: the following arguments are required: hostname/ip, port
```
```
$ knockserver
usage: knockserver [-h] <command> ...
knockserver: error: the following arguments are required: <command>
```
Entry points for each application are located in ```knock/__main__.py``` and ```knockserver/__main__.py``` respectively.
## Visual Studio Code
- Install the python extension (https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- Select Python 3.7.0 (pipenv) as your interpreter. CONTROL + SHIFT + P (or F1). Search "Select Interpreter" and select it. Select  Python 3.7.0 (pipenv) from the list of available interpreters.