import json

from cryptography.fernet import InvalidToken, Fernet
from knockserver.config import Config
from knockcommon.encryption import decrypt_multi_key, encrypt
from knockcommon.helpers import get_pass, print_verbose
from socket import socket, AF_INET, SOCK_DGRAM
from subprocess import Popen, DEVNULL, STDOUT
from threading import Timer
from time import time


def run(args):
    """Run the knockserver.

    :param argparse.Namespace args: arguments gathered from argparse
    """
    config = Config(args.config)

    with socket(AF_INET, SOCK_DGRAM) as listener:
        listener.bind((args.host, args.port))

        # Load configured client keys, and commands
        client_keys = config.get_client_keys()
        commands = config.commands.copy()
        recently_run = {}

        print("knockserver listening at %s:%d" % (args.host, args.port))
        while True:
            message, client = listener.recvfrom(65536)

            try:
                # Decrypt message
                client_name, decrypted_message = decrypt_multi_key(message, client_keys, args.age)

                # Decode JSON
                message_dict = json.loads(decrypted_message)

                # Check if it is a request
                if message_dict['type'] == "REQUEST":
                    # Run the command
                    cmd = message_dict['command']
                    if cmd in commands:
                        command = commands[cmd]

                        # Need decoded, but not decrypted message to get signature
                        timestamp, message_data = Fernet._get_unverified_token_data(message)

                        # Remove old values from recently run
                        cur_time = time()
                        for key, ran_at in list(recently_run.items()):
                            if cur_time - ran_at > args.age:
                                del recently_run[key]

                        # Check that this is not a replayed message
                        if message_data[-32:] not in recently_run:
                            if 'subprocess' not in command or command['subprocess'].poll() is not None:
                                command['subprocess'] = Popen(command['command'], stdout=DEVNULL, stderr=STDOUT,
                                                              cwd=command['directory'])
                                print_verbose("Started command '%s'" % cmd, args)

                            if command['teardown'] != 0:
                                if 'timer' in command:
                                    command['timer'].cancel()
                                command['timer'] = Timer(command['teardown'], subprocess_teardown,
                                                         (command['subprocess'],))
                                command['timer'].start()

                            # Everything worked, send an ACK
                            message_dict['type'] = "ACK"
                            ack_string = json.dumps(message_dict)
                            ack_message = encrypt(ack_string, client_keys[client_name])

                            listener.sendto(ack_message, client)
                            print_verbose("Sent ACK for command '%s'" % cmd, args)
                            print_verbose(("Sent Packet to %s:%d:\n" % client) + ack_message.hex(), args)

                            # Add signature to recently run
                            recently_run[message_data[-32:]] = time()
                        else:
                            print_verbose("Invalid Message Received: Message already processed", args)
                    else:
                        print_verbose("Invalid Message Received: Command '%s' does not exist" % cmd, args)
                else:
                    print_verbose("Invalid Message Received: was not a request", args)
            except InvalidToken:
                print_verbose("Invalid Message Received: Failed decryption", args)
            except Exception as e:
                print_verbose("Unexpected Error: %s" % str(e), args)


def subprocess_teardown(subprocess):
    """Kill a subprocess.

    :param subprocess.Popen subprocess: subprocess to teardown
    """
    subprocess.kill()
    subprocess.wait()


def add_client(args):
    """Add a client to knockserver.

    :param argparse.Namespace args: arguments gathered from argparse
    """
    config = Config(args.config)
    password = get_pass(args)

    config.add_client(args.name, password)

    print("Added key for client '%s'" % args.name)


def remove_client(args):
    """Remove a client from knockserver.

    :param argparse.Namespace args: arguments gathered from argparse
    """
    config = Config(args.config)

    if config.remove_client(args.name):
        print("Removed key for client '%s'" % args.name)
    else:
        print("Key for client '%s' is not stored" % args.name)


def add_command(args):
    """Add a command to knockserver.

    :param argparse.Namespace args: arguments gathered from argparse
    """
    config = Config(args.config)

    config.add_command(args.name, args.command, args.teardown, args.directory)

    print("Added command with name '%s'" % args.name)


def remove_command(args):
    """Remove a command from knockserver.

    :param argparse.Namespace args: arguments gathered from argparse
    """
    config = Config(args.config)

    if config.remove_command(args.name):
        print("Removed command with name '%s'" % args.name)
    else:
        print("Command with name '%s' is not stored" % args.name)
