import json

from cryptography.fernet import InvalidToken
from knockcommon.encryption import pass_to_fernet, encrypt, decrypt
from knockcommon.helpers import get_pass, print_verbose
from socket import socket, AF_INET, SOCK_DGRAM, timeout
from time import time


def send(args):
    """Send a knock.

    :param argparse.Namespace args: arguments gathered from argparse
    :return:
    """
    # Get password, convert into functional Fernet object
    password = get_pass(args) 
    key = pass_to_fernet(password)

    # Create the socket
    with socket(AF_INET, SOCK_DGRAM) as sender:
        # Set the timeout to the configured value
        sender.settimeout(args.timeout / 1000)

        # Create message object to send to server
        message = {
            "command": args.command,
            "type": "REQUEST"
        }

        # Convert the message to JSON
        message_string = json.dumps(message)

        # We wont accept and ACKs older than this
        # Cast to int because that is the precision fernet uses
        # Forgive up to age seconds old, for out of sync client/server clocks
        request_time = int(time()) - args.age
        for retry in range(args.retries):
            # We encrypt the message for each time, so that the creation time is up to date
            # Fernet automatically sets the creation time upon encryption
            message_encrypted = encrypt(message_string, key)

            # Send the encrypted message to the server
            sender.sendto(message_encrypted, (args.host, args.port))
            print_verbose("Sent Packet:\n" + message_encrypted.hex(), args)

            # Wait for an ACK
            try:
                received_message, server = sender.recvfrom(65536)

                # Check if the ACK is too old
                ack_time = key.extract_timestamp(received_message)
                if ack_time >= request_time:
                    received_message_decrypted = decrypt(received_message, key)
                    received_message_dict = json.loads(received_message_decrypted)

                    # Check if this is the right ACK
                    if received_message_dict['type'] == "ACK" and received_message_dict['command'] == args.command:
                        print("Command '%s' acknowledged" % received_message_dict['command'])

                        # Stop retrying
                        return
                else:
                    print_verbose("Received old ACK", args)
            except InvalidToken:
                # In the event that the ACK is malformed, continue to next retry
                print_verbose("Received malformed ACK", args)
            except timeout:
                print_verbose("ACK not received within timeout period", args)

        # If we got here, we never got an acknowledgment
        print("Knock failed, no acknowledgment received")
