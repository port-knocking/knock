from base64 import urlsafe_b64encode
from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def pass_to_fernet_key(password):
    """Expand a password into a fernet key.

    :param str password: a password to expand to a fernet key
    :return: the fernet key corresponding to the password
    :rtype: bytes
    """
    salt = b'\xd0\xf3xw\xea\xde\x7f\x9cU\xa6\x07FY\xef\xc1\xcb'
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend()
    )

    return urlsafe_b64encode(kdf.derive(password.encode()))


def pass_to_fernet(password):
    """Expand a password into a functional Fernet object.

    :param password: a password to expand to a fernet key
    :return: the Fernet object corresponding to the password
    :rtype: cryptography.fernet.Fernet
    """
    return Fernet(pass_to_fernet_key(password))


def encrypt(message, fernet):
    """Encrypts a message with the Fernet object.

    :param str message:
    :param cryptography.fernet.Fernet fernet:
    :return: the encrypted message
    :rtype: bytes
    """
    return fernet.encrypt(message.encode())


def decrypt(message, fernet, ttl=None):
    """Decrypts a message with the Fernet object.

    :param str message:
    :param cryptography.fernet.Fernet fernet:
    :param float ttl:
    :return: the decrypted message
    :rtype: str
    """
    return fernet.decrypt(message, ttl).decode()


def decrypt_multi_key(message, client_keys, ttl=None):
    """Decrypts a message using one of the client_keys.

    :param str message:
    :param dict client_keys:
    :param float ttl:
    :return: the nickname of the client and the decrypted message
    :rtype: tuple
    :raises: cryptography.fernet.InvalidToken
    """
    for name, fernet_key in client_keys.items():
        try:
            return name, decrypt(message, fernet_key, ttl)
        except InvalidToken:
            pass

    raise InvalidToken()
