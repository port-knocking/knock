from getpass import getpass


def get_pass(args):
    """Fetches a password from a user securely,
    if it was not parsed by argparse.

    :param argparse.Namespace args: arguments gathered from argparse
    :return: the user's password
    :rtype: str
    """
    if args.password is None:
        args.password = getpass()

    return args.password


def print_verbose(msg, args):
    """Print the string if verbose mode is enabled.

    :param str msg: string to print
    :param argparse.Namespace args: arguments gathered from argparse
    """
    if args.verbose:
        print(msg)
