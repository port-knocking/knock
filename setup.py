from setuptools import setup

setup(
    name='knock',
    version='0.0.1',
    packages=['knock', 'knockserver', 'knockcommon'],
    entry_points={
        'console_scripts': [
            'knock=knock.__main__:main',
            'knockserver=knockserver.__main__:main'
        ]
    }, install_requires=['cryptography'])
