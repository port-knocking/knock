import json

from cryptography.fernet import Fernet
from knockcommon.encryption import pass_to_fernet_key
from os.path import isfile


class Config:
    def __init__(self, filename):
        """Initialize config from filename.

        :param str filename:
        """
        self.filename = filename
        self.clients = {}
        self.commands = {}

        self.load_config_from_file()

    def add_client(self, name, password):
        """Add a client to the config file.

        :param str name:
        :param str password:
        """
        self.clients[name] = pass_to_fernet_key(password).decode()

        self.write_config_to_file()

    def remove_client(self, name):
        """Remove a client from the config file.

        :param str name:
        :return: whether the client was removed or not
        :rtype: bool
        """
        if name in self.clients:
            del self.clients[name]
            self.write_config_to_file()
            return True
        else:
            return False

    def add_command(self, name, command, teardown, directory):
        """Add a command to the config file.

        :param str name:
        :param str command:
        :param int teardown:
        :param str directory:
        """
        self.commands[name] = {
            'command': command,
            'teardown': teardown,
            'directory': directory
        }

        self.write_config_to_file()

    def remove_command(self, name):
        """Remove a command from the config file.

        :param str name:
        :return: whether the command was removed or not
        :rtype: bool
        """
        if name in self.commands:
            del self.commands[name]
            self.write_config_to_file()
            return True
        else:
            return False

    def get_client_keys(self):
        """Get the Fernet objects for each client.

        :return: dictionary containing client names and Fernet objects
        :rtype: dict
        """
        client_keys = {}
        for name, fernet_string in self.clients.items():
            client_keys[name] = Fernet(fernet_string)
        
        return client_keys

    def load_config_from_file(self):
        """Load the values from the config file.
        """
        if isfile(self.filename):
            try:
                with open(self.filename, 'r') as config_file:
                    config = json.load(config_file)

                self.clients = config["clients"]
                self.commands = config["commands"]
            except (IOError, json.JSONDecodeError):
                self.clients = {}
                self.commands = {}

                print('Warning: Config file exists but was not readable. Reverting to default settings.')
        else:
            self.clients = {}

    def write_config_to_file(self):
        """Save the values to the config file.
        """
        with open(self.filename, 'w') as config_file:
            json.dump({
                'clients': self.clients,
                'commands': self.commands
            }, config_file, indent=2)
