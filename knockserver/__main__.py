from argparse import ArgumentParser
from knockserver.knockserver import run, add_client, remove_client, add_command, remove_command
from os.path import join, abspath, dirname


def main():
    # Get command line options using argparse
    # Create the main parser
    parser = ArgumentParser(description='Run a server to look out for and react to knocks.')
    subparsers = parser.add_subparsers(metavar='<command>')

    # Make the command field required. Must be done this way due to a python bug.
    subparsers.required = True

    # Add parsing for shared arguments
    shared_parser = ArgumentParser(add_help=False)
    shared_parser.add_argument('-cfg', '--config', metavar='config',
                               help='file path to the config file for knockserver',
                               default=join(dirname(abspath(__file__)), "config.json"))

    # Add parsing for the 'run' command
    parser_run = subparsers.add_parser('run', parents=[shared_parser],
                                       help='Run the knockserver')
    parser_run.add_argument('-i', '--host', metavar='hostname/ip',
                            help='the hostname or ip to listen on',
                            default='0.0.0.0')
    parser_run.add_argument('-p', '--port', metavar='port', type=int,
                            help='the port for knockserver to listen on',
                            default=56625)
    parser_run.add_argument('-a', '--age', metavar='age',
                            help='the maximum age of a received knock in s', default=5, type=int)
    parser_run.add_argument('-v', '--verbose', action='store_true',
                            help='display verbose runtime information')
    parser_run.set_defaults(func=run)

    # Add parsing for the 'add-client' command
    parser_add_client = subparsers.add_parser('add-client', parents=[shared_parser],
                                              help='Add the key for a knock client')
    parser_add_client.add_argument('name', metavar='name',
                                   help='a nickname for the client associated with the key')
    parser_add_client.add_argument('-p', '--password', metavar='password',
                                   help='password used for sending knocks, used to generate key')
    parser_add_client.set_defaults(func=add_client)

    # Add parsing for the 'remove-client' command
    parser_remove_client = subparsers.add_parser('remove-client', parents=[shared_parser],
                                                 help='Remove the key for a knock client')
    parser_remove_client.add_argument('name', metavar='name',
                                      help='a nickname for the client associated with the key')
    parser_remove_client.set_defaults(func=remove_client)

    # Add parsing for the 'add-command' command
    parser_add_command = subparsers.add_parser('add-command', parents=[shared_parser],
                                               help='Add a command to execute upon successful knock')
    parser_add_command.add_argument('name', metavar='name',
                                    help='a nickname for the command')
    parser_add_command.add_argument('command', metavar='command',
                                    help='command to execute upon successful knock')
    parser_add_command.add_argument('-t', '--teardown', metavar='teardown', type=int,
                                    help='an amount time in seconds to force close the command',
                                    default=0)
    parser_add_command.add_argument('-d', '--directory', metavar='directory',
                                    help='the working directory in which to execute the command',
                                    default=None)
    parser_add_command.set_defaults(func=add_command)

    # Add parsing for the 'remove-command' command
    parser_remove_command = subparsers.add_parser('remove-command', parents=[shared_parser],
                                                  help='Remove a configured command')
    parser_remove_command.add_argument('name', metavar='name',
                                       help='a nickname for the command')
    parser_remove_command.set_defaults(func=remove_command)

    # Execute the selected command
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
